<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");
class Siswa extends CI_Controller {


	function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+7:00'");
        $waktu_sql = $this->db->query("SELECT NOW() AS waktu")->row_array();
        $this->waktu_sql = $waktu_sql['waktu'];
        $this->opsi = array("a","b","c","d","e");
	}
	public function get_servertime() {
		$now = new DateTime(); 
        $dt = $now->format("M j, Y H:i:s O"); 

        j($dt);
	}

	public function cek_aktif() {
		if ($this->session->userdata('admin_valid') == false && $this->session->userdata('admin_id') == "") {
			redirect('siswa/login');
		} 
	}
	function index(){
		$this->cek_aktif();
		
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		
		$a['p']			= "v_main";
		
		$this->load->view('aaa', $a);
	}

	public function login() {
		$this->load->view('aaa_login');
	}

	public function act_login() {
		
		$username	= $this->input->post('username');
		$password	= $this->input->post('password');
		
		$password2	= md5($password);
		
		$q_data		= $this->db->query("SELECT * FROM peserta WHERE username = '".$username."' AND password = '$password2'");
		$j_data		= $q_data->num_rows();
		$a_data		= $q_data->row();
		
		$_log		= array();
		if ($j_data === 1) {
			// $sess_nama_user = "";
			// if ($a_data->level == "siswa") {
			// 	$det_user = $this->db->query("SELECT nama FROM m_siswa WHERE id = '".$a_data->kon_id."'")->row();
			// 	if (!empty($det_user)) {
			// 		$sess_nama_user = $det_user->nama;
			// 	}
			// } else if ($a_data->level == "guru") {
			// 	$det_user = $this->db->query("SELECT nama FROM m_guru WHERE id = '".$a_data->kon_id."'")->row();
			// 	if (!empty($det_user)) {
			// 		$sess_nama_user = $det_user->nama;
			// 	}
			// } else {
			// 	$sess_nama_user = "Administrator Pusat";
			// }
			$data = array(
                    'id' => $a_data->id_peserta,
                    'id_kelas' => $a_data->id_kelas,
                    'admin_user' => $a_data->username,
                    'admin_level' => 'siswa',
                    'nama_peserta' => $a_data->nama_peserta,
                    'email' => $a_data->email,
                    'telp' => $a_data->telp,
                    'asal_sekolah' => $a_data->asal_sekolah,
                    'pendidikan' => $a_data->pendidikan,
                    'alamat' => $a_data->alamat,
					'valid' => true
                    );	
			// $data = array(
   //                  'admin_id' => $a_data->id,
   //                  'admin_user' => $a_data->username,
   //                  'admin_level' => $a_data->level,
   //                  'admin_konid' => $a_data->kon_id,
   //                  'admin_nama' => $sess_nama_user,
			// 		'admin_valid' => true
   //                  );
            $this->session->set_userdata($data);
			$_log['log']['status']			= "1";
			$_log['log']['keterangan']		= "Login berhasil";
			$_log['log']['detil_admin']		= $this->session->userdata;
		} else {
			$_log['log']['status']			= "0";
			$_log['log']['keterangan']		= "Maaf, username dan password tidak ditemukan";
			$_log['log']['detil_admin']		= null;
		}
		
		j($_log);
	}
}